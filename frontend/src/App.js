import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  const [apiResponse, setApiResponse] = useState('');

  useEffect(() => {
    fetch('http://backend:8000/users/')
      .then(response => response.json())
      .then(data => setApiResponse(data.username))
      .catch(error => console.error('There was an error!', error));
  }, []);

  return (
    <div className='App'>
      <header className='App-header'>
        <img src={logo} className='App-logo' alt='logo' />
        <p>Edit <code>src/App.js</code> and save to reload.</p>
        <p>Username from API: {apiResponse || 'Loading...'}</p>
        <a className='App-link' href='https://reactjs.org' target='_blank' rel='noopener noreferrer'>Learn React</a>
      </header>
    </div>
  );
}
export default App;
